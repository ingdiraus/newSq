package com.hackathon.squad4.squad4Demo.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hackathon.squad4.squad4Demo.dao.StockRepository;
import com.hackathon.squad4.squad4Demo.model.Stock;

@RestController
public class StockService {
	
	@Autowired
	StockRepository repository;

	@RequestMapping(value="/getAllStocks", method=RequestMethod.GET)
	public List<Stock> getAllStocks() {
		List<Stock> stocks = repository.findAll();
		return stocks;
	}
	
	@RequestMapping(value="/getStock", method=RequestMethod.GET)
	public Stock getStock(@PathVariable Long stockId) {
		Optional<Stock> stock = repository.findById(stockId);
		return stock.get();
	}
	
	@RequestMapping(value="/saveStocks", method=RequestMethod.POST)
	public String saveStocks(@RequestBody Stock stock) {
		repository.save(stock);
		return "success";
	}
}
